﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Models
{
    public class Nota
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string ContentNote { get; set; }
        public string DateUpdate { get; set; }
    }
}
