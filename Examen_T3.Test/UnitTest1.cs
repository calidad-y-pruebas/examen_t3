using NUnit.Framework;
using Examen_T3.Controllers;
using Examen_T3.Repository;
using Moq;
using Microsoft.AspNetCore.Mvc;
using Examen_T3.Models;
using System.Collections.Generic;

namespace Examen_T3.Test
{
    public class Tests
    {

        Mock<INotaRepository> mock;
        NotasController controller;
        Nota note;

        [SetUp]
        public void Setup()
        {
            mock = new Mock<INotaRepository>();
            controller = new NotasController(null, mock.Object);
            note = new Nota { Title = "Nota Two", ContentNote = "COntenido ", DateUpdate = "06/ene/2021" };
        }

        [Test]
        public void TestSearch()
        {
            var result = controller.Search("Hola") as ViewResult;

            Assert.IsInstanceOf<ViewResult>(result);
            Assert.IsTrue(controller.ModelState.IsValid);

            Nota noteTes = new Nota()
            {
                Id = 1,
                Title = "",
                ContentNote = "",
                DateUpdate = ""
            };

        }
        [Test]
        public void TestIndex()
        {
            Nota note = new Nota()
            {
                Id = 1,
                Title = "Nota 1",
                ContentNote = "Contenido de la nota 1",
                DateUpdate = ""
            };

            var result = controller.NewNote(note);

            Assert.AreNotEqual(note, result);
        }

        [Test]
        public void TestNewNote()
        {
            var note = new Nota { Title = "Nota Two", ContentNote = "COntenido ", DateUpdate = "06/ene/2021" };
            
            var tempMock = new Mock<INotaRepository>();
            //controller.TempData = mock.Object;

            var view = controller.NewNote(note);

            Assert.IsInstanceOf<RedirectToActionResult>(view);
            mock.Verify(o => o.RegisterNote(note), Times.Once);
        }
        [Test]
        public void TestUpdateNote()
        {
            var note = new Nota { Title = "Nota Editada", ContentNote = "Contenido editado ", DateUpdate = "06/ene/2021" };

            var tempMock = new Mock<INotaRepository>();
            //controller.TempData = mock.Object;

            var view = controller.UpdateNote(note);

            Assert.IsInstanceOf<RedirectToActionResult>(view);
            mock.Verify(o => o.UpdateNote(note), Times.Once);
        }
        [Test]
        public void Test3()
        {
            mock.Setup(o => o.SearchNote(It.IsAny<string>())).Returns(new List<Nota>());

            var controller = new NotasController(null,mock.Object);

            ViewResult view = controller.NewNote(note) as ViewResult;
            view = controller.Search("Hola") as ViewResult;

            
            Assert.IsInstanceOf<List<Nota>>(view.Model);
            Assert.AreEqual(0, view.Model);

        }
        [Test]
        public void TestRemoveNote()
        {


            RedirectToActionResult result = controller.DeleteNote(1) as RedirectToActionResult;

            Assert.IsInstanceOf<RedirectToActionResult>(result);
        }
    }
}