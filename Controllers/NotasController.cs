﻿using Examen_T3.DB;
using Examen_T3.Models;
using Examen_T3.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Controllers
{
    public class InfoNotes
    {
        public Nota note { get; set; }
        public string info { get; set; }
    }

    public class NotasController : Controller
    {
        private NotesContext context;
        private INotaRepository repository;

        public NotasController(NotesContext context, INotaRepository repository)
        {
            this.context = context;
            this.repository = repository;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var notes = GetInfNotes();
            return View("Index", notes);
        }
        [HttpGet]
        public IActionResult MuestraNota(int id)
        {
            var note = repository.GetNotes().FirstOrDefault(n => n.Id == id);
            return View("ViewNota", note);
        }
        [HttpGet]
        public IActionResult NewNote()
        {
            return View();
        }
        [HttpPost]
        public IActionResult NewNote(Nota note)
        {
            DateTime date = new DateTime();
            note.DateUpdate = date.ToString("dd,MMM,yyyy");

            repository.RegisterNote(note);

            return RedirectToAction("Index");
        }
        [HttpGet]
        public IActionResult DeleteNote(int id)
        {
            repository.DeleteNote(repository.GetNotes().FirstOrDefault(n => n.Id == id));
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult UpdateNote(int id)
        {
            Nota note = repository.GetNotes().FirstOrDefault(n => n.Id == id);

            return View("UpdateNote",note);
        }
        [HttpPost]
        public IActionResult UpdateNote(Nota note)
        {
            DateTime date = new DateTime();
            note.DateUpdate = date.ToString("dd,MMM,yyyy");

            note.DateUpdate = ReturnDate();

            repository.UpdateNote(note);
            return RedirectToAction("Index");
        }
        [HttpPost]
        public IActionResult Search(string Search)
        {
            var notes = repository.SearchNote(Search);

            return View("SearchResult", notes);
        }



        private string Return50C(String cadena)
        {
            string newCadena = "";
            if (cadena.Length <= 50)
                return cadena;
            else
            {
                for (int i = 0; i < 50; i++)
                {
                    newCadena += cadena[i].ToString();
                }
                return newCadena;
            }

        }
        private List<Nota> GetInfNotes(){
            var notes = repository.GetNotes().ToList();
            //var notes = context.Notes.ToList();
            Nota infNote;
            List<Nota> notesList = new List<Nota>();


            foreach (Nota nota in notes)
            {
                infNote = new Nota()
                {
                    Id = nota.Id,
                    Title = nota.Title,
                    ContentNote = Return50C(nota.ContentNote)
                };
                notesList.Add(infNote);
            }
            return notesList;
        }

        private string ReturnDate()
        {
            DateTime date = new DateTime();

            return date.ToString("dd,MMM,yyyy"); ;

        }
    }
}
