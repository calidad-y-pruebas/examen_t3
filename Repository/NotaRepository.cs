﻿using Examen_T3.DB;
using Examen_T3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.Repository
{
    public interface INotaRepository
    {
        public List<Nota> GetNotes();
        public void RegisterNote(Nota note);
        public void DeleteNote(Nota note);
        public void UpdateNote(Nota note);
        public List<Nota> SearchNote(String search);

    }
    public class NotaRepository : INotaRepository
    {
        private readonly NotesContext context;

        public NotaRepository(NotesContext context)
        {
            this.context = context;
        }

        public void DeleteNote(Nota note)
        {
            context.Notes.Remove(note);
            context.SaveChanges();
        }

        public List<Nota> GetNotes()
        {
            return context.Notes.ToList();
        }

        public void RegisterNote(Nota note)
        {
            context.Notes.Add(note);
            context.SaveChanges();
        }

        public void UpdateNote(Nota note)
        {
            context.Notes.Update(note);
            context.SaveChanges();
        }
        public List<Nota> SearchNote(String search)
        {
            var notes = context.Notes.Where(n => n.Title.Contains(search) || n.ContentNote.Contains(search)).ToList();

            return notes;
        }
    }
}
