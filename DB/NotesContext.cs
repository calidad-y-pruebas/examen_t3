﻿using Examen_T3.DB.Mapping;
using Examen_T3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Examen_T3.DB
{
    public class NotesContext: DbContext
    {
        public DbSet<Nota> Notes { get; set; }
        public NotesContext(DbContextOptions<NotesContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new NoteMap());
        }
    }
}
